// Problem 1: Complete the secondLargest function which takes in an array of numbers in input and return the second biggest number in the array. (without using sort)?
function secondLargest(array) {
  // Write your code here
  let currmax = -1;
  let place = -1
  for (let i in array){
    
    if(array[i] > currmax ){
      currmax = array[i]
      place = i;
      
    }
  }
  let secondmax = -1;
    let secondplace = -1;
    for (let i in array)
    {
        if(i == place){
            continue;
        }
        else{
            if(array[i] > secondmax ){
            secondmax = array[i]
            secondplace = i
        }
        }
    }
    return secondmax;
    

  
 }

// Problem 2: Complete the calculateFrequency function that takes lowercase string as input and returns frequency of all english alphabet. (using only array, no in-built function)
function calculateFrequency(string) {
  // Write your code here
  
  let frequency = {"1":0,};
  for (var i=0; i<string.length; i++) {
    if (string[i] === " ") 
      {
        continue;
      }
    else if(string[i] == string[i].toUpperCase()) {
      continue;
    }
    if (frequency.hasOwnProperty(string[i])) {
      frequency[string[i]] += 1
    }
    else {
      frequency[string[i]] = 1
    }
  }
  
  delete frequency["1"];
  console.log(frequency)
  return frequency;
  
}

// Problem 3: Complete the flatten function that takes a JS Object, returns a JS Object in flatten format (compressed)
function flatten(unflatObject) {
  // Write your code here
 let ans = {};
 
 for (let i in unflatObject) {
   if(!unflatObject.hasOwnProperty(i)) continue;
   
   if((typeof unflatObject[i]) == "object" && unflatObject[i] !==null) {
     let flatOb = flatten(unflatObject[i]);
     for(let x in flatOb) {
       if (!flatOb.hasOwnProperty(x)) continue;
       // console.log(flatOb)
       ans[i+ '.' + x] = flatOb[x];
     }
   }
   else {
     // console.log(ans[i])
     ans[i] = unflatObject[i];
     
   }
   
 }
  
  return ans;
  
}

// Problem 4: Complete the unflatten function that takes a JS Object, returns a JS Object in unflatten format
function unflatten(flatObject) {
  // Write your code here
 
}