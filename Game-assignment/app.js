document.addEventListener('DOMContentLoaded', ()=> {
   const ninja = document.querySelector('.ninja ')
   const gameDisplay= document.querySelector('.game');
   const ground = document.querySelector('.ground');

   let ninLeft = 220;
   let ninBottom = 100;

   let gravity = 2;

   let isGameOver = false;

   let gap = 440;

    function startGame() {

        ninBottom -= gravity;

        ninja.style.bottom = ninBottom + 'px';
        ninja.style.left = ninLeft + 'px';
    }

    // setInterval(startGame, 20)

    let timerinit = setInterval(startGame,30)
    
    function controls(e) {
        if (e.keyCode == 32) {
            jump()
        }
        // 32 is keycode for the space bar 

    }


    function jump() {
        
        if (ninBottom<500) {
        ninBottom += 50;
        }
        ninja.style.bottom = ninBottom + 'px';
        // console.log(ninBottom);
    }

    document.addEventListener('keyup',controls);


    function obstacle() {

        let stoneLeft = 500;
        
        
        let randomHeight = Math.random() * 80;
        
        let stoneBottom = randomHeight;

        const stone =  document.createElement('div');
        const topstone = document.createElement('div');
        
        if(!isGameOver)  {
            stone.classList.add('stone');
            topstone.classList.add('topstone')
        }
        gameDisplay.appendChild(stone);
        gameDisplay.appendChild(topstone);

        stone.style.left = stoneLeft + 'px';
        stone.style.bottom = stoneBottom +'px';
// top stone
        topstone.style.left = stoneLeft + 'px';
        topstone.style.bottom = stoneBottom + gap + 'px';

        function moveStone() {
            stoneLeft -= 2;
            stone.style.left = stoneLeft + 'px';
            topstone.style.left = stoneLeft + 'px';
            
            if (stoneLeft === -60) {
                clearInterval(timerId)
                gameDisplay.removeChild(stone)
                gameDisplay.removeChild(topstone)
            } 

            if( stoneLeft > 200 
                && stoneLeft< 280        
                && ninLeft === 220     
                && (ninBottom<stoneBottom + 150 ||
                    ninBottom > stoneBottom +gap -198     ) ||
                    ninBottom === 0  ) {
                gameOver()
                clearInterval(timerId)
            }

        }

        let timerId = setInterval(moveStone, 20);

        if(!isGameOver) setTimeout(obstacle,3000)

    }

    obstacle()

    function gameOver() {
        clearInterval(timerinit);
        console.log('game over :(');
        alert('Game Over   ')
        location.reload();
        isGameOver = true;
        document.removeEventListener('keyup',controls)
    }


} )