window.addEventListener('load', ()=> {
    const form = document.querySelector("#new-task");
    const input = document.querySelector("#task-input");
    const list_el = document.querySelector("#tasks");
    console.log(input);
    // console.log(form);

    form.addEventListener('submit', (e) => {
        e.preventDefault();

        // console.log("submit form");
        const task = input.value;

        if(!task) {
            alert("Please fill out the task");
            return;
        }

        const task_el = document.createElement("div");
        task_el.classList.add("task");

        const task_content = document.createElement("div")
        task_content.classList.add("content");
        // task_content.innerText = task;

        task_el.appendChild(task_content);

        const task_input = document.createElement("input");
        task_input.classList.add("text");
        task_input.type = "text";

        task_input.value = task;

        task_input.setAttribute("readonly", "readonly");

        task_content.appendChild(task_input);

        const task_action = document.createElement("div");
        task_action.classList.add("actions")

        const task_edit = document.createElement("button");
        task_edit.classList.add("edit");
        task_edit.innerHTML = "Edit";

        const task_delete = document.createElement("button")
        task_delete.classList.add("delete");
        task_delete.innerHTML = "Delete";

        task_action.appendChild(task_edit);
        task_action.appendChild(task_delete);

        task_el.appendChild(task_action);

        list_el.appendChild(task_el);

        input.value = "";
        
        task_edit.addEventListener("click", ()=> {
            if(task_edit.innerText.toLowerCase() == "edit") {
                task_input.removeAttribute("readonly");
                task_input.focus();
                task_edit.innerText = "Save";
            } 
            else {
                task_input.setAttribute("readonly","readonly");
                // console.log("Save");
                task_edit.innerText = "Edit";
            }

        })

        task_delete.addEventListener("click", ()=>{

            list_el.removeChild(task_el);

        })

    })

})